﻿using ServiciosWeb.Datos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiciosWep.WepApi.Controllers
{
    public class UsersController : ApiController
    {
        ExamAxityEntities DB = new ExamAxityEntities(); 

        /// <summary>
        /// Consulta informacion de Usuarios
        /// </summary>
        /// <returns></returns>
        
        [HttpGet]
        public IEnumerable<Users> Get() 
        {
            var listado = DB.Users.ToList();
            return listado;
        }

        [HttpGet]
        public Users Get(int id, string password)
        {
            var user = DB.Users.FirstOrDefault(x => x.IdUsuario == id && x.Password == password);
            return user;
        }
    }
}
