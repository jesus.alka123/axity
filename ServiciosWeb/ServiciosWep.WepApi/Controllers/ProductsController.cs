﻿using ServiciosWeb.Datos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ServiciosWep.WepApi.Controllers
{
    public class ProductsController : ApiController
    {
        ExamAxityEntities DB = new ExamAxityEntities();

        /// <summary>
        /// Consulta informacion de Usuarios
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        public IEnumerable<Products> Get()
        {
            var listado = DB.Products.ToList();
            return listado;
        }
    }
}
